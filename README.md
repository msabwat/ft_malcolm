Testing (peer evaluation)
========

In the subject, it is requested to only update the arp table with a spoofed mac address. To do this, we can run :

```
# listen to arp broadcasts from the target, and send an arp reply only to the source machine
docker-compose up attacker

# send a quick ping to a machine, and print the arp table before it is overwritten
docker-compose up victim
```


Testing (TODO)
========

0. first step is to build the project with `make`

In the context of this project, the source (attacker) is the machine we want to spoof. The target (victim) is the only machine that will see the impersonation. Then, the victim thinks it will send packets to the attacker, but instead, sends it to a third party.

1. open tcpdump:

```bash
docker-compose run third-party
```

2. launch the spoofer:

It will spoof the attacker address to the victim, the consequence is that packets sent to the attacker will be redirected to the third-party. The example is volontarily weird, and can be adapted if any host wants to impersonate another one. Impersonating a router you don't own is not a good idea.

```bash
docker-compose run attacker
```

It will hang because the program is waiting for an arp broadcast initiated by the target (the victim).

3. initiate the arp broadcast:

The victim will ping the attacker (to trigger the arp broadcast). To prevent the attacker from responding with an arp reply, arp is disabled in eth0 in the attacker container.
Once the spoofer sends the ARP Reply, we see that the ping is sent to the third-party instead of the attacker.

```bash
docker-compose run victim
```

This is to observe the situation: (confirmed by the tcpdump in the third-party shell)
- ICMP echo request is sent to the attacker
- ICMP redirect is sent from the third party (instead of the attacker)
