#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <errno.h>
#include <sys/socket.h>
#include <net/if.h>
#include <linux/if_arp.h>
#include <netinet/ip.h> 

#include "ft_malcolm.h"
#include "libft.h"

static char char_to_ahex(char c) {
    if (c < 10) {
        return c + 48;
    }
    else if ((c >= 10) && (c < 16)) {
        return c - 10 + 97;
    }
    // unreachable
    assert (0);
    return -1;
}

static int is_broadcast(t_eth_hdr *eth_hdr) {
    for (int i = 0; i < 6; i++) {
        if (eth_hdr->destination_mac[i] != 0xff) {
            return 0;
        }        
    }
    return 1;
}
/* inet_ntoa forbidden */
static void print_ip(in_addr_t ip) {
    char *tmp = 0;

    for (int i = 0; i < 3; i++) {
        tmp = ft_itoa((int)(ip & 255));
        ft_putstr(tmp);
        ft_putchar('.');
        free(tmp);
        ip = ip >> 8;
    }
    tmp = ft_itoa((int)(ip & 255));
    ft_putstr(tmp);
    free(tmp);
}

char *str_from_mac(void *bytes, bool byte_order) {
    char *result = ft_strnew(16);
    if (!result)
        return NULL;
    uint8_t *cur = (uint8_t *)bytes;
    uint8_t tmp = 0;
    for (int i = 0; i < 16; i += 3) {
        tmp = *cur;
        if (byte_order)
            result[i + 1] = char_to_ahex(tmp & 15);
        else
            result[i] = char_to_ahex(tmp & 15);
        tmp = tmp >> 4;
        if (byte_order)
            result[i] = char_to_ahex(tmp & 15);
        else
            result[i + 1] = char_to_ahex(tmp & 15);
        if (i + 2 < 16)
            result[i + 2] = ':';
        cur += 1;
    }
    return (result);
}

unsigned int get_ifindex(in_addr_t src_ip) {
    struct ifaddrs *interfaceArray = NULL;
    struct ifaddrs *tempIfAddr = NULL;
    int rc = getifaddrs(&interfaceArray);
    struct sockaddr_in *sa;
    in_addr_t tmp_ip;
    unsigned index = 1;
    if (rc == 0) {
        for( tempIfAddr = interfaceArray;
             tempIfAddr != NULL; tempIfAddr = tempIfAddr->ifa_next) {
            sa = (struct sockaddr_in *)tempIfAddr->ifa_addr;
            tmp_ip = sa->sin_addr.s_addr;
#ifdef DOCKER_TEST
            (void)src_ip;
            if (ft_strcmp(tempIfAddr->ifa_name, INTERFACE_NAME) == 0) {
                // printf("Index is: %d\n", tmp_ip);
#else
            if (src_ip == tmp_ip) {
#endif
                printf("Found interface: %s\n\n", tempIfAddr->ifa_name);
                freeifaddrs(interfaceArray);
                index += 1;
#ifdef DOCKER_TEST
                (void)index;
                return tmp_ip;
#else 
                return index;
#endif
            }
        }
        freeifaddrs(interfaceArray);
    }
    return 0;
}

static int match_src(t_arp_hdr *arp_hdr, in_addr_t ip_addr, uint8_t *mac_addr) {
    uint8_t tmp = 0;
    for (int i = 0; i < 4; i++) {
        tmp = arp_hdr->sender_ip[i];
        if (tmp != (ip_addr & 255))
            return 0;
        ip_addr = ip_addr >> 8;
    }
    for (int i = 0; i < 6; i++) {
        if (mac_addr[i] != arp_hdr->sender_mac[i])
            return 0;
    }
    return 1;
}

int recv_arp_broadcast_from(in_addr_t ip_addr, uint8_t *mac_addr) {
    char in[IP_MAXPACKET];

    int sock = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (sock == -1) {
        printf("error opening recv socket\n");
        return -1;
    }
    int ret = recvfrom(sock, in, IP_MAXPACKET, 0, NULL, NULL);
    if (ret == -1) {
        printf("error: recvfrom failed\n");
        close(sock);
        return -1;
    }
    uint8_t *in_ptr = (uint8_t *)in;
    t_eth_hdr *eth_hdr = (t_eth_hdr *)in_ptr;
    t_arp_hdr *arp_hdr = (t_arp_hdr *)(in_ptr + 14);

    if(is_broadcast(eth_hdr) && (ntohs(eth_hdr->eth_type) == 0x0806)){
        if (ntohs(arp_hdr->opcode) == 1) {
            if (match_src(arp_hdr, ip_addr, mac_addr)) {                
                // printf("great success\n");
                printf("An ARP request has been broadcast.\n");
                bool byte_order = (ntohs(0x4201) == 0x4201) ? false: true;
                printf("\tmac address of request %s\n", str_from_mac(mac_addr, byte_order));
                ft_putstr("\tIP address of request: ");
                print_ip(ip_addr);
                ft_putstr("\n");
                close(sock);
                return 1;
            }
        }
    }
    close(sock);
    return 0;
}
/*
  Kudos for this, it helped a lot for debugging.
  https://lynxbee.com/sending-arp-request-and-receiving-arp-reply-using-c-code/#.YiONfbko9hE
*/
int send_arp_reply(in_addr_t from_ip_addr, uint8_t *from_mac_addr, in_addr_t to_ip_addr, uint8_t *to_mac_addr) {
    struct sockaddr_ll device;
    t_arp_hdr arphdr;
    int frame_length = 0;
    uint8_t *ether_frame = (uint8_t *)ft_strnew(IP_MAXPACKET);
    if (!ether_frame)
        return -1;
    unsigned int index = get_ifindex(from_ip_addr);
    if (index == 0) {
        printf("get_ifindex: could not find device index\n");
        ft_strdel((char **)&ether_frame);
        return -1;
    }

    for (int i = 0; i < 6; i++) {
        arphdr.target_mac[i] = to_mac_addr[i];
        arphdr.sender_mac[i] = from_mac_addr[i];
    }

    in_addr_t from = from_ip_addr;
    in_addr_t to = to_ip_addr;
    for (int i = 0; i < 4; i++) {
        arphdr.sender_ip[i] = from & 255;
        arphdr.target_ip[i] = to & 255;
        from = from >> 8;
        to = to >> 8;
    }
    device.sll_ifindex = index;
    device.sll_family = AF_PACKET;
    ft_memcpy (device.sll_addr, arphdr.sender_mac, 6 * sizeof (uint8_t));
    device.sll_halen = htons (6);

    arphdr.htype = htons (1);
    arphdr.ptype = htons (ETH_P_IP);
    arphdr.hlen = 6;
    arphdr.plen = 4;
    arphdr.opcode = htons (ARPOP_REPLY);
    frame_length = 6 + 6 + 2 + ARP_HDRLEN;
    ft_memcpy (ether_frame, arphdr.target_mac, 6 * sizeof (uint8_t));
    ft_memcpy (ether_frame + 6, arphdr.sender_mac, 6 * sizeof (uint8_t));

    ether_frame[12] = ETH_P_ARP / 256;
    ether_frame[13] = ETH_P_ARP % 256;

    ft_memcpy (ether_frame + ETH_HDRLEN, &arphdr,
               ARP_HDRLEN * sizeof (uint8_t));

    int sock = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_ALL));
    if (sock == -1) {
        printf("Error could not create socket to send the arp reply\n");
        ft_strdel((char **)&ether_frame);
        return -1;
    }
    ssize_t ret = sendto(sock, ether_frame, frame_length, 0,
                         (struct sockaddr *)&device, sizeof(device));
    if (ret == -1) {
        printf("Error sending arp reply \n");
        ft_strdel((char **)&ether_frame);
        close (sock);
        return -1;
    }
    close (sock);
    ft_strdel((char **)&ether_frame);
    return 0;
}
