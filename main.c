#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <net/ethernet.h>

#include "ft_malcolm.h"

#include <assert.h>

t_sys *sys = NULL;

static void print_usage() {
    printf("wrong options \n");
    printf("usage:\n");
    printf("./ft_malcolm [Source IP] [Source Mac] [Target Ip] [Target Mac]\n");
}

static void handle_sigint(int num) {
    (void)num;
    sys->quit = true;
}

static unsigned char asciichar_to_hex(char c) {
	if ((c >= '0') && (c <= '9'))
		return (c - 48);
	else if ((c >= 'a') && (c <= 'f'))
		return (c - 97 + 10);
	// not a hex string (should never happen)
    	abort();
	return 255;
}

static int is_valid_hex(char c) {
	if ((c >= '0') && (c <= '9'))
		return 1;
	else if ((c >= 'a') && (c <= 'f'))
		return 1;
	return 0;
}

static uint8_t *add_mac_address(const char *addr) {
    uint8_t *result = (uint8_t *)malloc(sizeof(uint8_t) * 6);
    if (!result)
        return NULL;
    unsigned j = 0;
    for (unsigned i = 0; i < 6; i++) {
        result[i] = asciichar_to_hex(addr[j]) & 15;
        result[i] = result[i] << 4;
        result[i] = result[i] | asciichar_to_hex(addr[j + 1]);
        j += 3;
    }
    return result;
}

static int check_mac_address(const char *addr) {
    unsigned i = 0;
    unsigned j = 0;

    while (addr[i]) {
        if (i > 16)
            return 0;
        if (j == 2) {
            if (addr[i] != ':')
                return 0;
            j = -1;
        }
        else {
            if (!is_valid_hex(addr[i])) {                
                return 0;
            }
        }
        i++;
        j++;
    }
    if (i < 17)
        return 0;
    return 1;
}

static int init(int ac, char **av) {
    (void) ac, (void) av; 
    signal(SIGINT, &handle_sigint);
    uid_t uid = getuid();
	if (uid != 0) {
		printf("error: need root to use raw sockets \n");
		return 0;
	}
    in_addr_t src_ip = inet_addr(av[1]);
    if (src_ip == (in_addr_t)-1) {
        printf("error: %s is not a valid source ip address\n", av[1]);
        return 0;
    }
    if (!check_mac_address(av[2])) {
        printf("error: %s is not a valid source mac address\n", av[2]);
        return 0;
    }
    in_addr_t target_ip = inet_addr(av[3]);
    if (target_ip == (in_addr_t)-1) {
        printf("error: %s is not a valid target ip address\n", av[3]);
        return 0;
    }
    if (!check_mac_address(av[4])) {
        printf("error: %s is not a valid source mac address\n", av[4]);
        return 0;
    }
    sys = (t_sys *)malloc(sizeof(t_sys));
    if (!sys)
        return 0;
    sys->src_ip = src_ip;
    sys->target_ip = target_ip;
    sys->src_mac = add_mac_address(av[2]);
    if (!sys->src_mac) {
        free(sys);
        return 0;
    }
    sys->target_mac = add_mac_address(av[4]);
    if (!sys->target_mac) {
        free(sys->src_mac);
        free(sys);
        return 0;
    }
    sys->quit = false;
    return 1;
}

int main(int ac, char **av) {
    if (ac != 5) {
        print_usage();
        return 1;
    }
    if (!init(ac, av)) {
        printf("error: exiting! \n");
        return 1;
    }
    while(42) {
        if (sys->quit == true)
            break;
        int ret = recv_arp_broadcast_from(sys->target_ip, sys->target_mac);
        if (ret == -1)
            break; // there was an error, message is already printed
        else if (ret == 0) {
            // received a packet, but not THE packet
            continue;
        }
        else if (ret == 1) {
            printf("\nNow sending an ARP reply to the target address with spoofed source, please wait...\n");
            ret = send_arp_reply(sys->src_ip, sys->src_mac,
                                 sys->target_ip, sys->target_mac);
            printf("Sent an ARP reply packet, you may now check the arp table on the target\n\n");
            if (ret == -1) {
                printf("error sending arp reply\n");
            }
            break;
        }
    }
    printf("Exiting program ...\n");
    free(sys->src_mac);
    free(sys->target_mac);
    free(sys);

    return 0;
}
