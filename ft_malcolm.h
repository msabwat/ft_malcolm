#include <arpa/inet.h>
#include <stdbool.h>

#define INTERFACE_NAME "eth0"
#define ETH_HDRLEN 14
#define ARP_HDRLEN 28

typedef struct s_sys {
    bool quit;
    in_addr_t src_ip;
    uint8_t *src_mac;
    in_addr_t target_ip;
    uint8_t *target_mac;
} t_sys;

typedef struct s_arp_hdr {
    uint16_t htype;
    uint16_t ptype;
    uint8_t hlen;
    uint8_t plen;
    uint16_t opcode;
    uint8_t sender_mac[6];
    uint8_t sender_ip[4];
    uint8_t target_mac[6];
    uint8_t target_ip[4];
} t_arp_hdr;

typedef struct s_eth_hdr {
    uint8_t destination_mac[6];
    uint8_t source_mac[6];
    uint16_t eth_type;
} t_eth_hdr;

int recv_arp_broadcast_from(in_addr_t ip_addr, uint8_t *mac_addr);
int send_arp_reply(in_addr_t from_ip_addr, uint8_t *from_mac_addr,
    in_addr_t to_ip_addr, uint8_t *to_mac_addr);
char *str_from_mac(void *bytes, bool byte_order);
unsigned int get_ifindex(in_addr_t src_ip);
int working_send();
